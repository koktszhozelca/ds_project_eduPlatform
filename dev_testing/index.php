<!DOCTYPE html>
<html lang="en">
  <head>
    <?php
    require_once("../classes/Object.php");
    require_once("../classes/Crypto.php");
    require_once("../classes/Event.php");
    require_once("../classes/User.php");
    require_once("../classes/Privilege.php");
    require_once("../classes/Email.php");
    require_once("../classes/Registration.php");
    require_once("../config/Config.php");
    require_once("../database/DBClient.php");
    require_once("../librarys/phpcrypt/phpCrypt.php");
    require_once("../librarys/PHPMailer/src/PHPMailer.php");
    require_once("../librarys/PHPMailer/src/Exception.php");
    require_once("../librarys/PHPMailer/src/SMTP.php");
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/3.1.0/octicons.min.css">

    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <?php
    if($_SERVER['REQUEST_METHOD']==="POST"){
      $dbC_A->query("DELETE FROM Temp_User WHERE 1");
      $dbC_A->query("ALTER Table Temp_User AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM UserSignature WHERE 1");
      $dbC_A->query("ALTER Table UserSignature AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM Registration WHERE 1");
      $dbC_A->query("ALTER Table Registration AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM User WHERE 1");
      $dbC_A->query("ALTER Table User AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM Privilege WHERE 1");
      $dbC_A->query("ALTER Table Privilege AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM Event WHERE 1");
      $dbC_A->query("ALTER Table Event AUTO_INCREMENT = 1");

      $privilege = new Privilege("Normal User");
      $privilege->addSettings("Create Event")->addSettings("Update Event")->addSettings("Remove Event")->create($dbC_A);
      echo "Database cleared";
    }
    ?>
    <form action="#" method="post">
      <input type="submit" class="btn btn-danger" name="btnClear" value="Clear Database">
    </form>

    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
