<!DOCTYPE html>
<html lang="en">
  <head>
    <?php
    require_once("classes/Object.php");
    require_once("classes/UserSignature.php");
    require_once("classes/UserSession.php");
    require_once("classes/Crypto.php");
    require_once("classes/Event.php");
    require_once("classes/User.php");
    require_once("classes/Privilege.php");
    require_once("classes/Email.php");
    require_once("classes/Registration.php");
    require_once("config/Config.php");
    require_once("database/DBClient.php");
    require_once("librarys/phpcrypt/phpCrypt.php");
    require_once("librarys/PHPMailer/src/PHPMailer.php");
    require_once("librarys/PHPMailer/src/Exception.php");
    require_once("librarys/PHPMailer/src/SMTP.php");

    if(isset($_POST['clearEvent'])){
      $dbC_A->query("DELETE FROM Registration WHERE 1");
      $dbC_A->query("ALTER Table Registration AUTO_INCREMENT = 1");
      $dbC_A->query("DELETE FROM Reaction WHERE 1");
      $dbC_A->query("ALTER Table Reaction AUTO_INCREMENT = 1");
      $dbC_A->query("DELETE FROM Event WHERE 1");
      $dbC_A->query("ALTER Table Event AUTO_INCREMENT = 1");
      header("Location: #");
    } else if(isset($_POST['clearDatabase'])){
      $dbC_A->query("DELETE FROM Reaction WHERE 1");
      $dbC_A->query("ALTER Table Reaction AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM UserSession WHERE 1");
      $dbC_A->query("ALTER Table UserSession AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM Temp_User WHERE 1");
      $dbC_A->query("ALTER Table Temp_User AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM UserSignature WHERE 1");
      $dbC_A->query("ALTER Table UserSignature AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM Registration WHERE 1");
      $dbC_A->query("ALTER Table Registration AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM User WHERE 1");
      $dbC_A->query("ALTER Table User AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM Event WHERE 1");
      $dbC_A->query("ALTER Table Event AUTO_INCREMENT = 1");
      header("Location: #");
    } else if(isset($_POST['clearDatabaseNoUser'])){
      $dbC_A->query("DELETE FROM Reaction WHERE 1");
      $dbC_A->query("ALTER Table Reaction AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM Temp_User WHERE 1");
      $dbC_A->query("ALTER Table Temp_User AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM Registration WHERE 1");
      $dbC_A->query("ALTER Table Registration AUTO_INCREMENT = 1");

      $dbC_A->query("DELETE FROM Event WHERE 1");
      $dbC_A->query("ALTER Table Event AUTO_INCREMENT = 1");
      header("Location: #");
    }

    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home Page</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/3.1.0/octicons.min.css">

    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style media="screen">
      .logout {
        position: absolute;
        top: 5%;
        right: 5%;
      }
    </style>
  </head>
  <body>
    <h2>Home Page</h2>
    <button class="btn btn-primary logout" id="btnLogout" type="button" name="button">Logout</button>

    <div class="container" style="margin-top:5%;">
      <center>
        <pre style="height:100px;"><center>The test output goes here.</center></pre>
        <h3>Related to event</h3>
        <button id="btnCreateEvent" class="btn btn-primary form-control" style="width:60%;" type="button" name="button">Create an Event</button>
        <button id="btnFetchEvent" class="btn btn-primary form-control" style="width:60%; margin-top:2%;" type="button" name="button">Fetch Event List</button>
        <form action="#" method="post">
          <input class="btn btn-danger form-control" style="width:60%; margin-top:2%;" type="submit" name="clearEvent" value="Clear all events (Debug)">
        </form>
      </center>
    </div>

    <hr>

    <div class="container">
      <center>
        <pre style="height:100px;"><center>The test output goes here.</center></pre>
        <h3>Related to User</h3>
        <button id="btnFetchUserList" class="btn btn-success form-control" style="width:60%;" type="button" name="button">Fetch User List</button>
      </center>
    </div>

    <hr>

    <div class="container">
      <center>
        <pre style="height:100px;"><center>The test output goes here.</center></pre>
        <h3>Related to User and Event</h3>
          <button id="btnLikeEvent" class="btn btn-success form-control" style="width:60%;" type="button" name="button">Like</button>
          <button id="btnDisLikeEvent" class="btn btn-warning form-control" style="width:60%; margin-top:2%;" type="button" name="button">Dislike</button>
          <button id="btnRegisterEvent" class="btn btn-primary form-control" style="width:60%; margin-top:2%;" type="button" name="button">Register</button>
      </center>
    </div>

    <hr>

    <div class="container">
      <center>
        <h3>Clear Database</h3>
        <form action="#" method="post">
          <input class="btn btn-danger form-control" style="width:60%; margin-top:2%;" type="submit" name="clearDatabaseNoUser" value="Clear Database without User (Debug)">
          <input class="btn btn-danger form-control" style="width:60%; margin-top:2%;" type="submit" name="clearDatabase" value="Clear Database (Debug)">
        </form>
      </center>
    </div>

    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdn.rawgit.com/hagino3000/jquery-jsonrpc2.0/master/jquery.jsonrpc.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-sha256/0.8.0/sha256.js" charset="utf-8"></script>
    <script type="text/javascript">
    var eventObj;

    $("#btnLikeEvent").prop("disabled", true);
    $("#btnDisLikeEvent").prop("disabled", true);
    $("#btnRegisterEvent").prop("disabled", true);

    $.jsonrpc.defaultUrl = 'http://www2.comp.polyu.edu.hk/~15011089d/DS_Proj/API/APIHandler.php';

    $("#btnLogout").click(function(){
      var cipherToken = localStorage.getItem("session");
      var email = localStorage.getItem("email");
      unlockSession(cipherToken, email, function(token, userObj){
        var params = {
          token : token,
          userObj : JSON.stringify(userObj)
        };
        sendAPIRequest("logoutUser", params, function(result){
          if(result=="true") {
            localStorage.clear();
            window.location = <?php echo json_encode(Config::$loginPage);?>;
          }
        });
      });
    });

    $("#btnCreateEvent").click(function(){
      var cipherToken = localStorage.getItem("session");
      var email = localStorage.getItem("email");
      var eventObj = {
         id : null,
         name : "Test event",
         startDateTime : "2017-07-07 10:00:00",
         endDateTime : "2017-07-07 15:00:00",
         regStartDateTime : "2017-07-01 10:00:00",
         featuredPic : "featuredPic",
         creatorId : null,
         regEndDateTime : "2017-07-09 10:00:00",
         vacancy : 230,
         eventKey : null,
         description : "This is the description",
         score : null
      };
      unlockSession(cipherToken, email, function(token, userObj){
        var params = {
          token : token,
          userObj : JSON.stringify(userObj),
          eventObj : JSON.stringify(eventObj)
        };
        sendAPIRequest("createAnEvent", params, function(result){
          if(result=="true") showOutput($("#btnCreateEvent").parent().find("pre"), "Event is created, click fetch event list to see.");
        });
      });
    });

    $("#btnFetchEvent").click(function(){
      sendAPIRequest("fetchEventList", {privilegeId : 1}, function(result){
        if(result.length>0){
          showOutput($("#btnFetchEvent").parent().find("pre"), result);
          eventObj = result[0];
          $("#btnLikeEvent").prop("disabled", false);
          $("#btnDisLikeEvent").prop("disabled", false);
          $("#btnRegisterEvent").prop("disabled", false);
        } else {
          showOutput($("#btnFetchEvent").parent().find("pre"), "No event is created.");
        }
      });
    });

    $("#btnFetchUserList").click(function(){
      sendAPIRequest("fetchUserList", null, function(result){
        showOutput($("#btnFetchUserList").parent().find("pre") ,result);
      });
    });

    $("#btnLikeEvent").click(function(){
      var text = $(this).html();
      if(text=="Like") $(this).html("UnLike");
      else $(this).html("Like");
      var cipherToken = localStorage.getItem("session");
      var email = localStorage.getItem("email");
      unlockSession(cipherToken, email, function(token, userObj){
        var params = {
          token : token,
          userObj : JSON.stringify(userObj),
          eventObj : JSON.stringify(eventObj),
          delta : text=="Like"?+1:-1
        };
        sendAPIRequest("likeAnEvent", params, function(latestLikeCount){
          showOutput($("#btnLikeEvent").parent().find("pre"), "Latest like count of Event id = 1: " + latestLikeCount + "<br>" + "SELECT * FROM Reaction;");
        });
      });
    });

    $("#btnDisLikeEvent").click(function(){
      var text = $(this).html();
      if(text=="DisLike") $(this).html("Undo DisLike");
      else $(this).html("DisLike");
      var cipherToken = localStorage.getItem("session");
      var email = localStorage.getItem("email");
      unlockSession(cipherToken, email, function(token, userObj){
        var params = {
          token : token,
          userObj : JSON.stringify(userObj),
          eventObj : JSON.stringify(eventObj),
          delta : text!="DisLike"?+1:-1
        };
        sendAPIRequest("disLikeAnEvent", params, function(latestLikeCount){
          showOutput($("#btnDisLikeEvent").parent().find("pre"), "Latest disLike count of Event id = 1: " + latestLikeCount + "<br>" + 'SELECT * FROM Reaction;');
        });
      });
    });

    $("#btnRegisterEvent").click(function(){
      var text = $(this).html();
      if(text=="Register") $(this).html("Withdraw");
      else $(this).html("Register");
      var funcName = text=="Register"? "registerAnEvent" : "withdrawAnEvent";
      var cipherToken = localStorage.getItem("session");
      var email = localStorage.getItem("email");
      unlockSession(cipherToken, email, function(token, userObj){
        var params = {
          token : token,
          userObj : JSON.stringify(userObj),
          eventObj : JSON.stringify(eventObj)
        };
        sendAPIRequest(funcName, params, function(result){
          showOutput($("#btnRegisterEvent").parent().find("pre"), text + " Event id = 1: " + result + "<br>" + "SELECT * FROM Registration;");
        });
      });
    });


    function sendAPIRequest(funcName, params, callback){
      $.jsonrpc({
          jsonrpc:"2.0",
          method: funcName,
          params: params,
          id:0
      }).done(function(result) {
        callback(result);
      }).fail(function(error) {
        console.info('code:', error.code);
        console.info('message:', error.message);
      });
    }

    function showOutput(control, msg){
      output = JSON.stringify(msg).replace('"','');
      control.html("");
      control.html(output.replace("\"", ""));
    }

    function unlockSession(cipherToken, email, callback){
      $.post("./API/unlockSession.php", {
        sessionData : cipherToken.toString(),
        email : email
      }, function(data){
        var sessionToken = JSON.parse(data);
        token = sessionToken.token;
        userObj = sessionToken.userObj;
        callback(token, userObj);
      });
    }
    </script>
  </body>
</html>
