<?php
class User extends Object {
  public $id, $email, $password, $displayName, $creationDateTime, $privilegeId, $profilePic;

  public function __construct($email, $password, $displayName, $privilegeId, $profilePic){
    $this->email = $email;
    $this->password = $password;
    $this->displayName = $displayName;
    $this->privilegeId = $privilegeId;
    $this->profilePic = $profilePic;
  }

  public function create($dbC){
    $this->password = hash('sha256', $this->password);
    $sqlQuery = "INSERT INTO User (email, password, displayName, privilegeId, profilePic)
    VALUES (
      '".$this->email."',
      '".$this->password."',
      '".$this->displayName."',
      '".$this->privilegeId."',
      '".$this->profilePic."'
    )";
    $dbC->query($sqlQuery);
    $this->id = $dbC->lastInsertedId;
    $sqlQuery = "SELECT creationDateTime FROM User WHERE id=".$this->id;
    $dbC->query($sqlQuery)->next_record();
    $this->creationDateTime=$dbC->record['creationDateTime'];
    $this->genSignature($dbC);
    return $this;
  }

  public function update($dbC){
    $this->password = hash('sha256', $this->password);
    $sqlQuery = "UPDATE User
    SET email = '".$this->email."',
        password = '".$this->password."',
        displayName = '".$this->displayName."',
        privilegeId = '".$this->privilegeId."',
        profilePic = '".$this->profilePic."'
    WHERE id=".$this->id;
    $dbC->query($sqlQuery);
    $this->genSignature($dbC);
    return $this;
  }

  public static function parseJSON($jsonUserObj){
    $tmp = json_decode($jsonUserObj);
    $userObj = new User(
      $tmp->email,
      $tmp->password,
      $tmp->displayName,
      $tmp->privilegeId,
      $tmp->profilePic
    );
    $userObj->id=$tmp->id;
    $userObj->creationDateTime=$tmp->creationDateTime;
    return $userObj;
  }

  public static function fetchUserList($dbC){
    $userList = array();
    $sqlQuery = "SELECT * FROM User";
    $dbC->query($sqlQuery);
    while($dbC->next_record())
      array_push($userList, User::getUserByResult($dbC->record));
    return $userList;
  }

  public static function updateUser($dbC, $jsonUserObj){
    $userObj = User::parseJSON($jsonUserObj);
    if(!$userObj->id) return 0;
    $userObj->update($dbC);
  }

  public static function getUserById($dbC, $id){
    $sqlQuery = "SELECT * FROM User WHERE id=".$id;
    $dbC->query($sqlQuery)->next_record();
    return User::getUserByResult($dbC->record);
  }

  public static function getUserByEmail($dbC, $email){
    $sqlQuery = "SELECT * FROM User WHERE email='".$email."'";
    $dbC->query($sqlQuery)->next_record();
    if(!$dbC->num_rows) return 0;
    return User::getUserByResult($dbC->record);
  }

  private static function getUserByResult($result){
    $user = new User(
      $result['email'],
      $result['password'],
      $result['displayName'],
      $result['privilegeId'],
      $result['profilePic']
    );
    $user->id=$result['id'];
    $user->creationDateTime = $result['creationDateTime'];
    return $user;
  }

  /*
   *  Write the IV and Key to the table
   */
  private function genSignature($dbC){
    Crypto::genUserSignature($dbC, $this);
    return $this;
  }

  /*
   * Authentication
   * The signature is generated by AES_128_CTR(email, password, displayName, creationDateTime, privilegeId, profilePic)
   *
   * 1. Check if email exist, no return false, yes get userId.
   * 2. Get all fields except password.
   * 3. Get the iv, key, and signature.
   * 4. Encrypt the data and compare the signature.
   */
  public static function authentication($dbC, $email, $password){
    $password = hash('sha256', $password);
    if(($userObj=User::getUserByEmail($dbC, $email))){
      if(Crypto::verifyUserSignature($dbC, $userObj, $password))
          return Crypto::genSessionToken($dbC, $userObj);
    } else return 0;
  }

  public static function logoutUser($dbC, $userObj, $token){
    Crypto::clearSessionToken($dbC, $userObj, $token);
  }

  /*
   *  Delete the outstanding registration, over 24 hrs
   *  Check if the email is exist.
   */
  private static function isTempUserExist($dbC, $email){
    $sqlQuery = "DELETE FROM Temp_User WHERE DATE(creationDateTime) - DATE(NOW()) >=1";
    $dbC->query($sqlQuery);
    $sqlQuery = "SELECT COUNT(*) as count FROM Temp_User WHERE email='".$email."'";
    $dbC->query($sqlQuery)->next_record();
    $count = $dbC->record['count'];
    $dbC->wipe();
    return $count;
  }

  /*
   * Insert a new entry to Temp_User.
   */
  public static function registerNewUser($dbC, $email){
    if(User::isTempUserExist($dbC, $email) || User::getUserByEmail($dbC, $email)) return 0;
    $signature = hash("sha256", $email.explode(" ", microtime())[1]);
    $sqlQuery = "INSERT INTO Temp_User (email, signature)
    VALUES (
      '".$email."',
      '".$signature."'
    )";
    $dbC->query($sqlQuery);
    User::sendEmailConfirm($email, $signature);
    return $dbC->lastInsertedId;
  }

  private static function sendEmailConfirm($email, $signature){
    $subject = "[Do-Not-Reply] Verify your email address.";
    $body = "
    <body>
    <p>Hi <strong>".$email."</strong></p>
    <p>Please verify your email address in 24 hours.</p>
    <p>
      ".Config::$verifyUserEmailURL.$signature."
    </p>
    <br>
    <p>Regards,</p>
    <p>EduPlatform</p>
    </body>
    ";
    $emailObj = new Email($subject, $body);
    $emailObj->addReceiver($email);
    $emailObj->sendMail();
  }

  private static function sendEmailPassword($email, $password){
    $subject = "[Do-Not-Reply] Welcome to EduPlatform";
    $body = "
    <body>
    <p>Hi <strong>".$email."</strong></p>
    <p>Welcome to EduPlatform, please use the following credential to login.</p>
    <p>
      Login email address: ".$email."<br>
      Password: ".$password."<br>
    </p>
    <p><strong>Important</strong>Please be reminded that to change your password immediately.</p>
    <br>
    <p>Regards,</p>
    <p>EduPlatform</p>
    </body>
    ";
    $emailObj = new Email($subject, $body);
    $emailObj->addReceiver($email);
    $emailObj->sendMail();
  }

  private static function isSignatureExpired($dbC, $signature){
    $sqlQuery = "DELETE FROM Temp_User WHERE DATE(creationDateTime) - DATE(NOW()) >=1";
    $dbC->query($sqlQuery);
    $sqlQuery = "SELECT COUNT(*) as count FROM Temp_User WHERE signature='".$signature."'";
    $dbC->query($sqlQuery)->next_record();
    return $dbC->record['count'];
  }

  public static function verifyUserEmail($dbC, $signature){
    if(User::isSignatureExpired($dbC, $signature)) {
      $sqlQuery = "SELECT email FROM Temp_User WHERE signature='".$signature."'";
      $dbC->query($sqlQuery)->next_record();
      $email = $dbC->record['email'];
      $password = Crypto::genUserTempPassword();
      $user = new User($email, $password, explode("@", $email)[0], Config::$defaultPrivilege, "ProfilePic.png");
      $user->create($dbC);
      $sqlQuery = "DELETE FROM Temp_User WHERE signature='".$signature."'";
      $dbC->query($sqlQuery);
      User::sendEmailPassword($email, $password);
      return 1;
    } else return 0;
  }

  public function disLikeAnEvent($dbC, $token, $eventObj){

  }

  public function likeAnEvent($dbC, $token, $eventObj){

  }

  public function commentAnEvent($dbC, $token, $eventObj){

  }

  public function deleteAnEvent($dbC, $token, $eventObj){
    if(!Crypto::verifySessionToken($dbC, $this, $token)) {return 0;}
    return $eventObj->delete($dbC);
  }

  public function updateAnEvent($dbC, $token, $eventObj){
    if(!Crypto::verifySessionToken($dbC, $this, $token)) {return 0;}
    $eventObj->update($dbC, $this);
    return $eventObj->id>0;
  }

  public function createAnEvent($dbC, $token, $eventObj){
    if(!Crypto::verifySessionToken($dbC, $this, $token)) {return 0;}
    $eventObj->create($dbC, $this);
    return $eventObj->id>0;
  }

  //Registration an event
  public function registerAnEvent($dbC, $token, $eventObj){
    if(!Crypto::verifySessionToken($dbC, $this, $token)) {return 0;}
    $registration = new Registration($eventObj->id, $this->id);
    $registration->create($dbC);
    return $registration->id>0;
  }

  //Withdraw an event registration
  public function withdrawAnEvent($dbC, $token, $eventObj){
    if(!Crypto::verifySessionToken($dbC, $this, $token)) {return 0;}
    $registration = Registration::getRegistrationByEventUserId($dbC, $eventObj->id, $this->id);
    if($registration) {
      $registration->delete($dbC);
      return 1;
    } return 0;
  }
}

?>
