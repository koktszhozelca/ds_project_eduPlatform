<?php
use PHP_Crypt\PHP_Crypt as PHP_Crypt;

class Crypto extends Object {

  public static function genUserTempPassword(){
    $key  = openssl_random_pseudo_bytes(10);
    return bin2hex($key);
  }

  public static function genUserSignature($dbC, $userObj){
    $data = "";
    $data .= $userObj->email;
    $data .= $userObj->password;
    $data .= $userObj->displayName;
    $data .= $userObj->creationDateTime;
    $data .= $userObj->privilegeId;
    $data .= $userObj->profilePic;
    Crypto::aesCTR_Encrypt($data)->create($dbC, $userObj);
  }

  public static function verifyUserSignature($dbC, $userObj, $password){
    $userSignature = UserSignature::getUserSignatureByUserId($dbC, $userObj->id);
    $data = "";
    $data .= $userObj->email;
    $data .= $password;
    $data .= $userObj->displayName;
    $data .= $userObj->creationDateTime;
    $data .= $userObj->privilegeId;
    $data .= $userObj->profilePic;
    $key  = hex2bin($userSignature->key);
    $iv = hex2bin($userSignature->iv);
    $signature = hex2bin($userSignature->signature);
    $crypt = new PHP_Crypt($key, PHP_Crypt::CIPHER_AES_128, PHP_Crypt::MODE_CTR);
    $crypt->IV($iv);
    $signaturePrime = $crypt->encrypt($data);
    return strcmp($signaturePrime, $signature)==0;
  }

  private static function getUserIdByEmail($dbC, $email){
    $sqlQuery = "SELECT id FROM User WHERE email='".$email."'";
    $dbC->query($sqlQuery)->next_record();
    if($dbC->num_rows) return $dbC->record['id'];
    else return 0;
  }

  /*
   *  Return a session token to the user when the authentication is session.
   *  The session token should be able to have a time factor.
   *  Token = AES(timeStamp), where key is user signature{0:20} + user's password{0:12}.
   *
   *  To verify the session token, decrypt the token by user signature{0:20} + user's password{0:12}.
   *
   *  If expired, redirect to login page.
   */
  public static function genSessionToken($dbC, $userObj){
    if(!($userSignature = UserSignature::getUserSignatureByUserId($dbC, $userObj->id))) return 0;
    $signature = hex2bin($userSignature->signature);
    $time = date("Y-m-d H:i:s", strtotime("+30 minutes"));
    $key = substr($signature, 0, 20).substr($userObj->password, 0, 12);
    $crypt = new PHP_Crypt($key);
    $sessionToken = bin2hex($crypt->encrypt($time));
    Crypto::updateSessionTable($dbC, $userObj->id, $sessionToken);
    $data = new UserSession($sessionToken, $userObj);
    $crypt = new PHP_Crypt(substr($userObj->password, 0, 16));
    $cipher = $crypt->encrypt($data->getSerialized());
    return bin2hex($cipher);
  }

  public static function verifySessionToken($dbC, $userObj, $token){
    if(!($userSignature = UserSignature::getUserSignatureByUserId($dbC, $userObj->id))) return 0;
    $token = hex2bin($token);
    $signature = hex2bin($userSignature->signature);
    $key = substr($signature, 0, 20).substr($userObj->password, 0, 12);
    $crypt = new PHP_Crypt($key);
    $time = $crypt->decrypt($token);
    return round((strtotime(date("Y-m-d H:i:s")) - strtotime($time))/3600, 1) < 30;
  }

  public static function clearSessionToken($dbC, $userObj, $token){
    if(Crypto::verifySessionToken($dbC, $userObj, $token))
      Crypto::removeSession($dbC, $userObj->id);
  }

  private static function updateSessionTable($dbC, $userId, $token){
    Crypto::removeSession($dbC, $userId);
    $sqlQuery = "INSERT INTO UserSession (userId, sessionToken) VALUES (
      ".$userId.",
      '".$token."'
    )";
    $dbC->query($sqlQuery);
    return $dbC->lastInsertedId>0;
  }

  private static function removeSession($dbC, $userId){
    $sqlQuery = "DELETE FROM UserSession WHERE userId=".$userId;
    $dbC->query($sqlQuery);
  }

  public static function aesCTR_Encrypt($plainText){
    $key  = openssl_random_pseudo_bytes(32);
    $crypt = new PHP_Crypt($key, PHP_Crypt::CIPHER_AES_128, PHP_Crypt::MODE_CTR);
    $iv = $crypt->createIV();
    $cipher = $crypt->encrypt($plainText);
    return new UserSignature($key, $iv, $cipher);
  }

  public static function aesCTR_Decrypt($aesEncrypted){
    $crypt = new PHP_Crypt($aesEncrypted->key, PHP_Crypt::CIPHER_AES_128, PHP_Crypt::MODE_CTR);
    $crypt->IV($aesEncrypted->iv);
    return $crypt->decrypt(hex2bin($aesEncrypted->cipher));
  }
}
?>
