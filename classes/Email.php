<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class Email extends Object {
  private $smtpServer = "smtp.gmail.com";
  private $sender = "comp3335ds@gmail.com", $password = "comp3335dspw", $displayName = "EduPlatform";
  private $mail = "", $receivers = array(), $subject = "", $body = "";

  public function __construct($subject, $body){
    $this->setup($subject, $body);
  }

  public function sendMail(){
    try{
      foreach ($this->receivers as $email) $this->mail->addAddress($email);
      $this->mail->isHTML(true);
      $this->mail->Subject = $this->subject;
      $this->mail->Body    = $this->body;
      $this->mail->AltBody = 'AltBody';
      $this->mail->send();
    } catch(Exception $e){
      echo 'Send Mail Error: ' . $this->mail->ErrorInfo;
    }
    return $this;
  }

  public function addReceiver($receiverEmail){
    array_push($this->receivers, $receiverEmail);
    return $this;
  }

  private function setup($subject, $body){
    try {
        $this->subject = $subject;
        $this->body = $body;
        $this->mail = new PHPMailer(true);
        // $this->mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $this->mail->isSMTP();                                      // Set mailer to use SMTP
        $this->mail->Host = $this->smtpServer;  // Specify main and backup SMTP servers
        $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
        $this->mail->Username = $this->sender;                 // SMTP username
        $this->mail->Password = $this->password;                           // SMTP password
        $this->mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port = 587;                                    // TCP port to connect to
        $this->mail->setFrom($this->sender, $this->displayName);
    } catch (Exception $e) {
        echo 'Mail configuration Error: ' . $this->mail->ErrorInfo;
    }
    return $this;
  }
}
?>
