<?php
class UserSession extends Object {
    public $token, $userObj;

    public function __construct($token, $userObj){
      $this->token = $token;
      $this->userObj = $userObj;
    }

    public function getSerialized(){
      return json_encode($this);
    }
}
?>
