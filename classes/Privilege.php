<?php
class Privilege extends Object {
    public $id, $settings=array(), $label;

    public function __construct($label){
      $this->label = $label;
    }

    public function addSettings($setting){
      array_push($this->settings, $setting);
      return $this;
    }

    public function create($dbC){
      $sqlQuery = "INSERT INTO Privilege (settings, label)
      VALUES (
        '".json_encode($this->settings)."',
        '".$this->label."'
      )";
      $dbC->query($sqlQuery);
      $this->id=$dbC->lastInsertedId;
      return $this;
    }

    public function update($dbC){
      $sqlQuery = "UPDATE Privilege
      SET settings = '".json_encode($this->settings)."',
          label = '".$this->label."'
      WHERE id = ".$this->id;
      $dbC->query($sqlQuery);
      return $this;
    }

    private static function getPrivilegeByResult($result){
      $privilege = new Privilege(
        $result['label']
      );
      $privilege->settings = json_decode($result['settings']);
      $privilege->id = $result['id'];
      return $privilege;
    }

    public static function getPrivilegeById($dbC, $id){
      $sqlQuery = "SELECT * FROM Privilege WHERE id=".$id;
      $dbC->query($sqlQuery)->next_record();
      return getPrivilegeByResult($dbC->record);
    }
}
?>
