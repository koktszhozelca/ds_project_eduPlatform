<?php
class Reaction {
  public $id, $eventId, $likeCount, $disLikeCount;

  private function __construct($eventId){
    $this->eventId = $eventId;
    $this->likeCount = 0;
    $this->disLikeCount = 0;
  }

  private function create($dbC){
    $sqlQuery = "INSERT INTO Reaction (eventId) VALUES (".$this->eventId.")";
    $dbC->query($sqlQuery);
    $this->id = $dbC->lastInsertedId;
    return $this;
  }

  public static function startReceiveReaction($dbC, $eventObj){
    $reaction = new Reaction($eventObj->id);
    return $reaction->create($dbC)->id>0;
  }

  public static function updateLikeCount($dbC, $eventObj, $userObj, $delta){
    $sqlQuery = "UPDATE Reaction SET likeCount=likeCount+".$delta." WHERE eventId=".$eventObj->id;
    $dbC->query($sqlQuery);
    if($delta == -1){
      $sqlQuery = "DELETE FROM ReactionBoard WHERE eventId=".$eventObj->id." AND userId=".$userObj->id;
      $dbC->query($sqlQuery);
    } else {
      $sqlQuery = "INSERT INTO ReactionBoard (eventId, userId, reaction)
      VALUES (
        ".$eventObj->id.",
        ".$userObj->id.",
        'like'
      )";
      $dbC->query($sqlQuery);
    }
    $sqlQuery = "SELECT likeCount FROM Reaction WHERE eventId=".$eventObj->id;
    $dbC->query($sqlQuery)->next_record();
    return $dbC->record['likeCount'];
  }

  public static function updateDisLikeCount($dbC, $eventObj, $userObj, $delta){
    $sqlQuery = "UPDATE Reaction SET disLikeCount=disLikeCount+".$delta." WHERE eventId=".$eventObj->id;
    $dbC->query($sqlQuery);
    if($delta == -1){
      $sqlQuery = "DELETE FROM ReactionBoard WHERE eventId=".$eventObj->id." AND userId=".$userObj->id;
      $dbC->query($sqlQuery);
    } else {
      $sqlQuery = "INSERT INTO ReactionBoard (eventId, userId, reaction)
      VALUES (
        ".$eventObj->id.",
        ".$userObj->id.",
        'dislike'
      )";
      $dbC->query($sqlQuery);
    }
    $sqlQuery = "SELECT disLikeCount FROM Reaction WHERE eventId=".$eventObj->id;
    $dbC->query($sqlQuery)->next_record();
    return $dbC->record['disLikeCount'];
  }

}

?>
