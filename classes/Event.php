<?php
class Event extends Object {
  public $id, $name, $startDateTime, $endDateTime, $regStartDateTime, $featuredPic, $creatorId;
  public $regEndDateTime, $vacancy, $eventKey, $description, $score;

  public function __construct($name, $startDateTime, $endDateTime, $regStartDateTime, $regEndDateTime, $vacancy, $description, $featuredPic){
    $this->name = $name;
    $this->startDateTime = $startDateTime;
    $this->endDateTime = $endDateTime;
    $this->regStartDateTime = $regStartDateTime;
    $this->regEndDateTime = $regEndDateTime;
    $this->vacancy = $vacancy;
    $this->description = $description;
    $this->featuredPic = $featuredPic;
  }

  public static function parseJSON($jsonEventObj){
    $tmp = json_decode($jsonEventObj);
    $event = new Event(
      $tmp->name,
      $tmp->startDateTime,
      $tmp->endDateTime,
      $tmp->regStartDateTime,
      $tmp->regEndDateTime,
      $tmp->vacancy,
      $tmp->description,
      $tmp->featuredPic
    );
    $event->id=$tmp->id;
    $event->eventKey = $tmp->eventKey;
    $event->score = $tmp->score;
    $event->creatorId = $tmp->creatorId;
    return $event;
  }

  private function genEventKey(){
    $data = "";
    $data.=$this->name;
    $data.=$this->startDateTime;
    $data.=$this->endDateTime;
    $data.=$this->regStartDateTime;
    $data.=$this->regEndDateTime;
    $data.=$this->vacancy;
    $data.=$this->description;
    $data.=$this->featuredPic;
    return hash('sha256', $data);
  }

  public function create($dbC, $userObj){
    $this->score = explode(" ", microtime())[1];
    $this->eventKey = $this->genEventKey();
    $sqlQuery = "INSERT INTO Event
      (name, startDateTime, endDateTime, regStartDateTime, regEndDateTime, vacancy, description, score, eventKey, featuredPic, creatorId)
      VALUES (
        '".$this->name."',
        '".$this->startDateTime."',
        '".$this->endDateTime."',
        '".$this->regStartDateTime."',
        '".$this->regEndDateTime."',
        ".$this->vacancy.",
        '".$this->description."',
        ".$this->score.",
        '".$this->eventKey."',
        '".$this->featuredPic."',
        ".$userObj->id."
    );";
    $dbC->query($sqlQuery);
    $this->id = $dbC->lastInsertedId;
    $this->creatorId = $userObj->id;
    Reaction::startReceiveReaction($dbC, $this);
    return $this;
  }

  public function update($dbC, $userObj){
    $this->eventKey = $this->genEventKey();
    $sqlQuery = "UPDATE Event
        SET name = '".$this->name."',
        startDateTime = '".$this->startDateTime."',
        endDateTime = '".$this->endDateTime."',
        regStartDateTime = '".$this->regStartDateTime."',
        regEndDateTime = '".$this->regEndDateTime."',
        vacancy = ".$this->vacancy.",
        description = '".$this->description."',
        score = ".$this->score.",
        eventKey = '".$this->eventKey."',
        featuredPic = '".$this->featuredPic."',
        creatorId = ".$userObj->id."
        WHERE id=".$this->id.";";
    $dbC->query($sqlQuery);
    $this->creatorId=$userObj->id;
    return $this;
  }

  public function delete($dbC){
    $sqlQuery = "SELECT COUNT(*) as count FROM Event WHERE
    regStartDateTime <= (NOW() - INTERVAL 1 DAY) AND id=".$this->id;
    $dbC->query($sqlQuery)->next_record();
    if($dbC->record['count']) return 0;
    $sqlQuery = "DELETE FROM Event WHERE id=".$this->id;
    $dbC->query($sqlQuery);
    return 1;
  }

  private static function getEventByResult($result){
    $event = new Event(
      $result['name'],
      $result['startDateTime'],
      $result['endDateTime'],
      $result['regStartDateTime'],
      $result['regEndDateTime'],
      $result['vacancy'],
      $result['description'],
      $result['featuredPic']
    );
    $event->id = $result['id'];
    $event->eventKey = $result['eventKey'];
    $event->score = $result['score'];
    $event->creatorId = $result['creatorId'];
    return $event;
  }

  public static function getEventById($dbC, $id){
    $sqlQuery = "SELECT * FROM Event WHERE id=".$id;
    $dbC->query($sqlQuery)->next_record();
    $event = Event::getEventByResult($dbC->record);
    $dbC->wipe();
    return $event;
  }

  public static function getEventByEventKey($dbC, $eventKey){
    $sqlQuery = "SELECT * FROM Event WHERE eventKey='".$eventKey."'";
    $dbC->query($sqlQuery)->next_record();
    $event = Event::getEventByResult($dbC->record);
    $dbC->wipe();
    return $event;
  }

  public static function fetchEventList($dbC, $privilegeId){
    $eventList=array();
    $sqlQuery = "SELECT * FROM Event WHERE privilegeId<=".$privilegeId;
    $dbC->query($sqlQuery);
    while($dbC->next_record())
      array_push($eventList, Event::getEventByResult($dbC->record));
    return $eventList;
  }
}

?>
