<?php
class UserSignature extends Object {
  public $id, $key, $iv, $signature;

  public function __construct($key, $iv, $signature){
    $this->key=$key;
    $this->iv=$iv;
    $this->signature=$signature;
  }

  private function removeOld($dbC, $userObj){
    $sqlQuery = "DELETE FROM UserSignature WHERE `userId` = ".$userObj->id;
    $dbC->query($sqlQuery);
    return $this;
  }

  public function create($dbC, $userObj){
    $this->removeOld($dbC, $userObj);
    $sqlQuery = "INSERT INTO UserSignature (`userId`, `iv`, `key`, `signature`)
    VALUES (
      ".$userObj->id.",
      '".bin2hex($this->iv)."',
      '".bin2hex($this->key)."',
      '".bin2hex($this->signature)."'
    )";
    $dbC->query($sqlQuery);
    $this->id = $dbC->lastInsertedId;
    return $this;
  }

  private static function getUserSignatureByResult($result){
    return new UserSignature(
      $result['key'],
      $result['iv'],
      $result['signature']
    );
  }

  public static function getUserSignatureByUserId($dbC, $userId){
    $sqlQuery = "SELECT * FROM UserSignature WHERE `userId`=".$userId;
    $dbC->query($sqlQuery)->next_record();
    return UserSignature::getUserSignatureByResult($dbC->record);
  }
}
?>
