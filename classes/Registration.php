<?php
class Registration extends Object {
    public $id, $eventId, $userId;

    public function __construct($eventId, $userId){
      $this->eventId=$eventId;
      $this->userId=$userId;
    }

    public function create($dbC){
      $sqlQuery = "INSERT INTO Registration (eventId, userId)
      VALUES (
        ".$this->eventId.",
        ".$this->userId."
      )";
      $dbC->query($sqlQuery);
      $this->id=$dbC->lastInsertedId;
      return $this;
    }

    public function update($dbC){
      $sqlQuery = "UPDATE Registration
      SET eventId = ".$this->eventId.",
          userId = ".$this->userId."
      WHERE id = ".$this->id;
      $dbC->query($sqlQuery);
      return $this;
    }

    public function delete($dbC){
      $sqlQuery = "DELETE FROM Registration WHERE id=".$this->id;
      $dbC->query($sqlQuery);
    }

    private static function getRegistrationByResult($result){
      $registration = new Registration($result['eventId'], $result['userId']);
      $registration->id = $result['id'];
      return $registration;
    }

    public static function getRegistrationByEventUserId($dbC, $eventId, $userId){
      $sqlQuery = "SELECT * FROM Registration WHERE eventId=".$eventId. " AND userId=".$userId;
      $dbC->query($sqlQuery)->next_record();
      return Registration::getRegistrationByResult($dbC->record);
    }

    public static function getRegistrationById($dbC, $id){
      $sqlQuery = "SELECT * FROM Registration WHERE id=".$id;
      $dbC->query($sqlQuery)->next_record();
      return Registration::getRegistrationByResult($dbC->record);
    }
}
?>
