<?php
require_once("includes.php");
use PHP_Crypt\PHP_Crypt as PHP_Crypt;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>API Test</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/3.1.0/octicons.min.css">

    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <pre>Demo Account: comp3335dsdemo@gmail.com<br>Gmail password: comp3335dspw<br>EduPlatform password: 7767841cb80db88ee90f</pre>

    <div class="container">
      <span class="label label-default">Email</span>
      <input id="txtEmail" class="form-control" type="text" name="" value="">
      <span class="label label-default">Password</span>
      <input id="txtPassword" class="form-control" type="password" name="" value="">
      <button id="btnAuth" class="btn btn-success" type="button" name="button">Login</button>
      <button id="btnRegistration" class="btn btn-primary" type="button" name="button">Registration</button>
    </div>

    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdn.rawgit.com/hagino3000/jquery-jsonrpc2.0/master/jquery.jsonrpc.js"></script>
    <script type="text/javascript">
    $.jsonrpc.defaultUrl = 'http://www2.comp.polyu.edu.hk/~15011089d/DS_Proj/API/APIHandler.php';

    $("#btnRegistration").click(function(){
      sendAPIRequest("UserRegistration", {email : $("#txtEmail").val()}, function(result){
        console.log(result);
        alert("A mail is sent to " + $("#txtEmail").val());
      });
    });

    $("#btnAuth").click(function(){
      var params = {
        email : $("#txtEmail").val(),
        password : $("#txtPassword").val()
      };
      sendAPIRequest("Authentication", params, function(result){
        console.log(result);
        if(result!="null"){
          localStorage.setItem('session', result);
          localStorage.setItem('email', $("#txtEmail").val());
          window.location = <?php echo json_encode(Config::$homePage); ?>;
        }
        else alert("Login failure");
      }, function(){
        alert("Login failure")
      });
    });

    function sendAPIRequest(funcName, params, successCallBack, failureCallBack){
      $.jsonrpc({
          jsonrpc:"2.0",
          method: funcName,
          params: params,
          id:0
      }).done(function(result) {
        successCallBack(result);
      }).fail(function(error) {
        failureCallBack();
        console.info('code:', error.code);
        console.info('message:', error.message);
      });
    }
    </script>
  </body>
</html>
