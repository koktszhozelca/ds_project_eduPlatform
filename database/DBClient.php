<?php
Class DBClient extends Object {
  private $hostname="", $username="", $password="", $dbname="";
  public $record = "", $num_rows = 0, $connection = "", $tempStorage = "", $lastInsertedId = "";

  public function __construct($hostname, $username, $password, $dbname){
    $this->hostname = $hostname;
    $this->username = $username;
    $this->password = $password;
    $this->dbname = $dbname;
  }

  public function query($sqlQuery){
    $this->open();
    $this->tempStorage = $this->connection->query($sqlQuery) or die("MySQL Error: " . $sqlQuery . "<br>Return: " . mysqli_error($this->connection));
    $this->lastInsertedId = $this->connection->insert_id;
    $this->close();
    return $this;
  }

  public function next_record(){
    $data = $this->tempStorage->fetch_assoc();
    $this->record = $data;
    $this->num_rows = sizeof($data);
    return $data;
  }

  public function wipe(){
    $this->tempStorage->free();
  }

  private function open(){
    $this->connection = new mysqli($this->hostname, $this->username, $this->password, $this->dbname);
  }

  private function close(){
    $this->connection->close();
  }
}
/*
  Initialize the variable directly when this file is included.
*/
$dbC_A = new DBClient(Config::$dbHost,Config::$dbUser,Config::$dbPW,Config::$dbName);
$dbC_B = new DBClient(Config::$dbHost,Config::$dbUser,Config::$dbPW,Config::$dbName);
$dbC_C = new DBClient(Config::$dbHost,Config::$dbUser,Config::$dbPW,Config::$dbName);
$dbC_D = new DBClient(Config::$dbHost,Config::$dbUser,Config::$dbPW,Config::$dbName);
$dbC_E = new DBClient(Config::$dbHost,Config::$dbUser,Config::$dbPW,Config::$dbName);
?>
