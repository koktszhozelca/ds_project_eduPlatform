<?php
require_once("includes.php");
$jsonrpc = json_decode(file_get_contents('php://input'), true);

// if($jsonrpc['method'] == "withdrawAnEvent"){
//   $param = $jsonrpc['params'];
//   $token = $param['token'];
//   $userJSON = $param['userObj'];
//   $eventJSON = $param['eventObj'];
//   $userObj = User::parseJSON($userJSON);
//   $eventObj = Event::parseJSON($eventJSON);
//   $result = $userObj->withdrawAnEvent($dbC, $token, $eventObj);
//   echo buildSuccessResponse($result, 1);
//   exit;
// }

$id = $jsonrpc['id'];
switch ($jsonrpc['method']) {
  //Related to user
  case 'Authentication':
    authentication($dbC_A, $jsonrpc['params'], $id);
    break;
  case 'UserRegistration':
    userRegistration($dbC_A, $jsonrpc['params'], $id);
    break;
  case 'VerifyUserEmail':
    verifyUserEmail($dbC_A, $jsonrpc['params'], $id);
    break;
  case 'logoutUser':
    logoutUser($dbC_A, $jsonrpc['params'], $id);
    break;

  //Related to User and Event
  case 'createAnEvent':
    createAnEvent($dbC_A, $jsonrpc['params'], $id);
    break;
  case 'updateAnEvent':
    updateAnEvent($dbC_A, $jsonrpc['params'], $id);
    break;
  case 'deleteAnEvent':
    deleteAnEvent($dbC_A, $jsonrpc['params'], $id);
    break;

  case 'likeAnEvent':
    likeAnEvent($dbC_A, $jsonrpc['params'], $id);
    break;
  case 'disLikeAnEvent':
    disLikeAnEvent($dbC_A, $jsonrpc['params'], $id);
    break;
  case 'commentAnEvent':
    commentAnEvent($dbC_A, $jsonrpc['params'], $id);
    break;

  case 'registerAnEvent':
    registerAnEvent($dbC_A, $jsonrpc['params'], $id);
    break;
  case 'withdrawAnEvent':
    withdrawAnEvent($dbC_A, $jsonrpc['params'], $id);
    break;

  //Fetching data
  case 'fetchEventList':
    fetchEventList($dbC_A, $jsonrpc['params'], $id);
    break;

  case 'fetchUserList':
    fetchUserList($dbC_A, $jsonrpc['params'], $id);
    break;

  default:
    echo json_encode(
      array(
        "jsonrpc"=>"2.0",
        "error"=>array(
            "code"=>-1,
            "message"=>"Method is not defined.",
            "id"=>$id
          )
      )
    );
    break;
}

function fetchEventList($dbC, $param, $id){
  $privilegeId = $param['privilegeId'];
  $eventList = Event::fetchEventList($dbC, $privilegeId);
  ob_clean();
  echo buildSuccessResponse($eventList, $id);
}

function fetchUserList($dbC, $param, $id){
  $eventList = User::fetchUserList($dbC);
  ob_clean();
  echo buildSuccessResponse($eventList, $id);
}

function commentAnEvent($dbC, $param, $id){
  $token = $param['token'];
  $userJSON = $param['userObj'];
  $eventJSON = $param['eventObj'];
  $comment = $oaram['comment'];
  $eventObj = Event::parseJSON($eventJSON);
  $userObj = User::parseJSON($userJSON);
  $result = CommentBoard::commentAnEvent($dbC, $eventObj, $userObj, $comment);
  if($result)  echo buildSuccessResponse("true",$id);
  else echo buildFailureResponse(-8, "Comment event failure.", $id);
}

/*
 *  dislike 1, remove dislike -1
 */
function disLikeAnEvent($dbC, $param, $id){
  $token = $param['token'];
  $userJSON = $param['userObj'];
  $eventJSON = $param['eventObj'];
  $delta = $param['delta'];
  $eventObj = Event::parseJSON($eventJSON);
  $userObj = User::parseJSON($userJSON);
  $latestDislistCount = Reaction::updateDisLikeCount($dbC, $eventObj, $userObj, $delta);
  echo buildSuccessResponse($latestDislistCount,$id);
}

/*
 *  Like 1, unlike -1
 */
function likeAnEvent($dbC, $param, $id){
  $token = $param['token'];
  $userJSON = $param['userObj'];
  $eventJSON = $param['eventObj'];
  $delta = $param['delta'];
  $eventObj = Event::parseJSON($eventJSON);
  $userObj = User::parseJSON($userJSON);
  $latestListCount = Reaction::updateLikeCount($dbC, $eventObj, $userObj, $delta);
  echo buildSuccessResponse($latestListCount,$id);
}

function deleteAnEvent($dbC, $param, $id){
  $token = $param['token'];
  $userJSON = $param['userObj'];
  $eventJSON = $param['eventObj'];
  $eventObj = Event::parseJSON($eventJSON);
  $userObj = User::parseJSON($userJSON);
  $result = $userObj->deleteAnEvent($dbC, $token, $eventObj);
  if($result)  echo buildSuccessResponse("true",$id);
  else echo buildFailureResponse(-7, "Delete event failure.", $id);
}

function updateAnEvent($dbC, $param, $id){
  $token = $param['token'];
  $userJSON = $param['userObj'];
  $eventJSON = $param['eventObj'];
  $eventObj = Event::parseJSON($eventJSON);
  $userObj = User::parseJSON($userJSON);
  $result = $userObj->updateAnEvent($dbC, $token, $eventObj);
  if($result)  echo buildSuccessResponse("true",$id);
  else echo buildFailureResponse(-6, "Update event failure.", $id);
}

function createAnEvent($dbC, $param, $id){
  $token = $param['token'];
  $userJSON = $param['userObj'];
  $eventJSON = $param['eventObj'];
  $eventObj = Event::parseJSON($eventJSON);
  $userObj = User::parseJSON($userJSON);
  $result = $userObj->createAnEvent($dbC, $token, $eventObj);
  if($result)  echo buildSuccessResponse("true",$id);
  else echo buildFailureResponse(-5, "Create event failure.", $id);
}

function withdrawAnEvent($dbC, $param, $id){
  $token = $param['token'];
  $userJSON = $param['userObj'];
  $eventJSON = $param['eventObj'];
  $eventObj = Event::parseJSON($eventJSON);
  $userObj = User::parseJSON($userJSON);
  $result = $userObj->withdrawAnEvent($dbC, $token, $eventObj);
  if($result)  echo buildSuccessResponse("true",$id);
  else echo buildFailureResponse(-4, "Withdraw event failure.", $id);
}

/*
 * Build an event object, then create.
 * The parameters should contains
 * SessionToken is required
 */
function registerAnEvent($dbC, $param, $id){
  $token = $param['token'];
  $userJSON = $param['userObj'];
  $eventJSON = $param['eventObj'];
  $eventObj = Event::parseJSON($eventJSON);
  $userObj = User::parseJSON($userJSON);
  $result = $userObj->registerAnEvent($dbC, $token, $eventObj);
  if($result)  echo buildSuccessResponse("true",$id);
  else echo buildFailureResponse(-4, "Register event failure.", $id);
}

function logoutUser($dbC, $param, $id){
  $token = $param['token'];
  $userJSON = $param['userObj'];
  $userObj = User::parseJSON($userJSON);
  User::logoutUser($dbC, $userObj, $token);
  echo buildSuccessResponse("true",$id);
}

function verifyUserEmail($dbC, $param, $id){
  $signature = $param['signature'];
  $result = User::verifyUserEmail($dbC, $signature);
  if($result) echo buildSuccessResponse("true",$id);
  else echo buildFailureResponse(-2, "The signature is expired.", $id);
}

function userRegistration($dbC, $param, $id){
  $email = $param['email'];
  $result = User::registerNewUser($dbC, $email);
  if($result) echo buildSuccessResponse("true",$id);
  else echo buildFailureResponse(-2, "The email had been registered.", $id);
}

function authentication($dbC, $param, $id){
  $email = $param['email'];
  $password = $param['password'];
  $result = json_encode(User::authentication($dbC, $email, $password));
  if($result && $result!="null")
    echo buildSuccessResponse($result,$id);
  else echo buildFailureResponse(-3,"login failure",$id);
}

function buildFailureResponse($errCode, $msg, $id){
  return json_encode(
    array(
      "jsonrpc"=>"2.0",
      "error"=>array(
          "code"=>$errCode,
          "message"=>$msg,
          "id"=>$id
        )
    )
  );
}

function buildSuccessResponse($msg, $id){
  return json_encode(
    array(
        "jsonrpc"=>"2.0",
        "result" => $msg,
        "id"=>$id
    )
  );
}
?>
